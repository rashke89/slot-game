var ar = ['akali.ico', 'akali.ico', 'akali.ico', 'annie.ico', 'annie.ico', 'brand.ico', 'brand.ico',
    'annie.ico', 'annie.ico', 'annie.ico', 'annie.ico', 'annie.ico', 'annie.ico', 'annie.ico',
    'annie.ico', 'draven.png', 'draven.png', 'draven.png', 'draven.png', 'fiora.ico',
    'fiora.ico', 'fiora.ico', 'fiora.ico', 'fiora.ico', 'fiora.ico', 'fiora.ico', 'fiora.ico',
    'lee.ico', 'lee.ico', 'lee.ico', 'lee.ico', 'lee.ico', 'lee.ico', 'olaf.ico', 'olaf.ico', 'olaf.ico', 'olaf.ico',
    'thresh.png', 'thresh.png', 'zed.png', 'zed.png','bonus.png','wanted.png'
];

function fillColumns() {
    for (var i = 0; i < 21; i++) {
        kolona1.append('<div class="box"></div>');
        kolona2.append('<div class="box"></div>');
        kolona3.append('<div class="box"></div>');
        kolona4.append('<div class="box"></div>');
        kolona5.append('<div class="box"></div>');
    }
}

function startTheSpin() {
    testZaBoje = 0;
    brojPogodjenihLinija = 0;
    console.clear();
    ukupanSpinWin = 0;
    vremeAnimacije = 0;
    win.html(ukupanSpinWin + "$");
    test1 = 0;
    checkStake();
    addNewColumns();
    fillNewColumns();
    startAnimation();
}

function addNewColumns() {
    for (var i = 1; i < 6; i++) {
        container.append('<div class="newnew newColumns' + i + '"</div>');
    }
}

function startAnimation() {
    test = 0;
    kolona1.animate({
        top: "+=4200px"
    }, 1000, function () {
        $(this).remove();
    })
    $('.newColumns1').animate({
        top: "+=4200px"
    }, 1000, function () {

    })
    kolona2.animate({
        top: "+=4200px"
    }, 1200, function () {
        $(this).remove();
    })
    $('.newColumns2').animate({
        top: "+=4200px"
    }, 1200, function () {

    })
    kolona3.animate({
        top: "+=4200px"
    }, 1400, function () {
        $(this).remove();
    })
    $('.newColumns3').animate({
        top: "+=4200px"
    }, 1400, function () {

    })
    kolona4.animate({
        top: "+=4200px"
    }, 1600, function () {
        $(this).remove();
    })
    $('.newColumns4').animate({
        top: "+=4200px"
    }, 1600, function () {

    })
    kolona5.animate({
        top: "+=4200px"
    }, 1800, function () {
        $(this).remove();
    })
    $('.newColumns5').animate({
        top: "+=4200px"
    }, 1800, function () {
        test++;
        if (test === 1) {
            $('.delete').remove();
            $('.newnew').addClass('delete');
            checkLines();
        }
        ;


    })
}

function fillNewColumns() {
    for (var i = 0; i < 21; i++) {
        var rand = Math.floor(Math.random() * ar.length);
        var rand1 = Math.floor(Math.random() * ar.length);
        var rand2 = Math.floor(Math.random() * ar.length);
        var rand3 = Math.floor(Math.random() * ar.length);
        var rand4 = Math.floor(Math.random() * ar.length);
        $('.newColumns1').append('<div class="box"><img src="img/' + ar[rand] + '"></div>');
        $('.newColumns2').append('<div class="box"><img src="img/' + ar[rand1] + '"></div>');
        $('.newColumns3').append('<div class="box"><img src="img/' + ar[rand2] + '"></div>');
        $('.newColumns4').append('<div class="box"><img src="img/' + ar[rand3] + '"></div>');
        $('.newColumns5').append('<div class="box"><img src="img/' + ar[rand4] + '"></div>');
    }
}

function checkLines() {
    $('.newColumns1').find('.box').eq(0).addClass('prva1');
    $('.newColumns1').find('.box').eq(1).addClass('prva2');
    $('.newColumns1').find('.box').eq(2).addClass('prva3');

    $('.newColumns2').find('.box').eq(0).addClass('druga1');
    $('.newColumns2').find('.box').eq(1).addClass('druga2');
    $('.newColumns2').find('.box').eq(2).addClass('druga3');

    $('.newColumns3').find('.box').eq(0).addClass('treca1');
    $('.newColumns3').find('.box').eq(1).addClass('treca2');
    $('.newColumns3').find('.box').eq(2).addClass('treca3');

    $('.newColumns4').find('.box').eq(0).addClass('cetvrta1');
    $('.newColumns4').find('.box').eq(1).addClass('cetvrta2');
    $('.newColumns4').find('.box').eq(2).addClass('cetvrta3');

    $('.newColumns5').find('.box').eq(0).addClass('peta1');
    $('.newColumns5').find('.box').eq(1).addClass('peta2');
    $('.newColumns5').find('.box').eq(2).addClass('peta3');


    var prva1 = $('.prva1');
    var prva2 = $('.prva2');
    var prva3 = $('.prva3');

    var druga1 = $('.druga1');
    var druga2 = $('.druga2');
    var druga3 = $('.druga3');

    var treca1 = $('.treca1');
    var treca2 = $('.treca2');
    var treca3 = $('.treca3');

    var cetvrta1 = $('.cetvrta1');
    var cetvrta2 = $('.cetvrta2');
    var cetvrta3 = $('.cetvrta3');

    var peta1 = $('.peta1');
    var peta2 = $('.peta2');
    var peta3 = $('.peta3');


    var prva1_src = prva1.find('img').attr('src');
    var prva2_src = prva2.find('img').attr('src');
    var prva3_src = prva3.find('img').attr('src');

    var druga1_src = druga1.find('img').attr('src');
    var druga2_src = druga2.find('img').attr('src');
    var druga3_src = druga3.find('img').attr('src');

    var treca1_src = treca1.find('img').attr('src');
    var treca2_src = treca2.find('img').attr('src');
    var treca3_src = treca3.find('img').attr('src');

    var cetvrta1_src = cetvrta1.find('img').attr('src');
    var cetvrta2_src = cetvrta2.find('img').attr('src');
    var cetvrta3_src = cetvrta3.find('img').attr('src');

    var peta1_src = peta1.find('img').attr('src');
    var peta2_src = peta2.find('img').attr('src');
    var peta3_src = peta3.find('img').attr('src');


    //prva
    if (prva1_src == druga1_src && druga1_src == treca1_src && treca1_src == cetvrta1_src && cetvrta1_src == peta1_src) {
        animateWin(prva1, druga1, treca1, cetvrta1, peta1);
    } else if (prva1_src == druga1_src && druga1_src == treca1_src && treca1_src == cetvrta1_src) {
        animateWin(prva1, druga1, treca1, cetvrta1);
    } else if (prva1_src == druga1_src && druga1_src == treca1_src) {
        animateWin(prva1, druga1, treca1);
    }
    //druga
    if (prva2_src == druga2_src && druga2_src == treca2_src && treca2_src == cetvrta2_src && cetvrta2_src == peta2_src) {
        animateWin(prva2, druga2, treca2, cetvrta2, peta2);
    } else if (prva2_src == druga2_src && druga2_src == treca2_src && treca2_src == cetvrta2_src) {
        animateWin(prva2, druga2, treca2, cetvrta2);
    } else if (prva2_src == druga2_src && druga2_src == treca2_src) {
        animateWin(prva2, druga2, treca2);
    }
    //treca
    if (prva3_src == druga3_src && druga3_src == treca3_src && treca3_src == cetvrta3_src && cetvrta3_src == peta3_src) {
        animateWin(prva3, druga3, treca3, cetvrta3, peta3);
    } else if (prva3_src == druga3_src && druga3_src == treca3_src && treca3_src == cetvrta3_src) {
        animateWin(prva3, druga3, treca3, cetvrta3);
    } else if (prva3_src == druga3_src && druga3_src == treca3_src) {
        animateWin(prva3, druga3, treca3);
    }


    //cetvrta 1
    if (prva1_src == druga2_src && druga2_src == treca3_src && treca3_src == cetvrta2_src && cetvrta2_src == peta1_src) {
        animateWin(prva1, druga2, treca3, cetvrta2, peta1)
    } else if (prva1_src == druga2_src && druga2_src == treca3_src && treca3_src == cetvrta2_src) {
        animateWin(prva1, druga2, treca3, cetvrta2)
    } else if (prva1_src == druga2_src && druga2_src == treca3_src) {
        animateWin(prva1, druga2, treca3)
    }
    //cetvrta 2
    if (prva3_src == druga2_src && druga2_src == treca1_src && treca1_src == cetvrta2_src && cetvrta2_src == peta3_src) {
        animateWin(prva3, druga2, treca1, cetvrta2, peta3)
    } else if (prva3_src == druga2_src && druga2_src == treca1_src && treca1_src == cetvrta2_src) {
        animateWin(prva3, druga2, treca1, cetvrta2)
    } else if (prva3_src == druga2_src && druga2_src == treca1_src) {
        animateWin(prva3, druga2, treca1)
    }


    //peta 1
    if (prva2_src == druga3_src && druga3_src == treca3_src && treca3_src == cetvrta3_src && cetvrta3_src == peta2_src) {
        animateWin(prva2, druga3, treca3, cetvrta3, peta2)
    } else if (prva2_src == druga3_src && druga3_src == treca3_src && treca3_src == cetvrta3_src) {
        animateWin(prva2, druga3, treca3, cetvrta3)
    } else if (prva2_src == druga3_src && druga3_src == treca3_src) {
        animateWin(prva2, druga3, treca3)
    }
    //peta 2
    if (prva2_src == druga1_src && druga1_src == treca1_src && treca1_src == cetvrta1_src && cetvrta1_src == peta2_src) {
        animateWin(prva2, druga1, treca1, cetvrta1, peta2)
    } else if (prva2_src == druga1_src && druga1_src == treca1_src && treca1_src == cetvrta1_src) {
        animateWin(prva2, druga1, treca1, cetvrta1)
    } else if (prva2_src == druga1_src && druga1_src == treca1_src) {
        animateWin(prva2, druga1, treca1)
    }


    //sesta 1
    if (prva3_src == druga3_src && druga3_src == treca2_src && treca2_src == cetvrta1_src && cetvrta1_src == peta1_src) {
        animateWin(prva3, druga3, treca2, cetvrta1, peta1)
    } else if (prva3_src == druga3_src && druga3_src == treca2_src && treca2_src == cetvrta1_src) {
        animateWin(prva3, druga3, treca2, cetvrta1)
    } else if (prva3_src == druga3_src && druga3_src == treca2_src) {
        animateWin(prva3, druga3, treca2)
    }
    // sesta 2
    if (prva1_src == druga1_src && druga1_src == treca2_src && treca2_src == cetvrta3_src && cetvrta3_src == peta3_src) {
        animateWin(prva1, druga1, treca2, cetvrta3, peta3)
    } else if (prva1_src == druga1_src && druga1_src == treca2_src && treca2_src == cetvrta3_src) {
        animateWin(prva1, druga1, treca2, cetvrta3)
    } else if (prva1_src == druga1_src && druga1_src == treca2_src) {
        animateWin(prva1, druga1, treca2)
    }


    // sedma 1
    if (prva1_src == druga1_src && druga1_src == treca2_src && treca2_src == cetvrta1_src && cetvrta1_src == peta1_src) {
        animateWin(prva1, druga1, treca2, cetvrta1, peta1)
    } else if (prva1_src == druga1_src && druga1_src == treca2_src && treca2_src == cetvrta1_src) {
        animateWin(prva1, druga1, treca2, cetvrta1)
    } else if (prva1_src == druga1_src && druga1_src == treca2_src) {
        animateWin(prva1, druga1, treca2)
    }
    // sedma 2
    if (prva3_src == druga3_src && druga3_src == treca2_src && treca2_src == cetvrta3_src && cetvrta3_src == peta3_src) {
        animateWin(prva3, druga3, treca2, cetvrta3, peta3)
    } else if (prva3_src == druga3_src && druga3_src == treca2_src && treca2_src == cetvrta3_src) {
        animateWin(prva3, druga3, treca2, cetvrta3)
    } else if (prva3_src == druga3_src && druga3_src == treca2_src) {
        animateWin(prva3, druga3, treca2)
    }


    // osma 1
    if (prva1_src == druga2_src && druga2_src == treca2_src && treca2_src == cetvrta2_src && cetvrta2_src == peta1_src) {
        animateWin(prva1, druga2, treca2, cetvrta2, peta1)
    } else if (prva1_src == druga2_src && druga2_src == treca2_src && treca2_src == cetvrta2_src) {
        animateWin(prva1, druga2, treca2, cetvrta2)
    } else if (prva1_src == druga2_src && druga2_src == treca2_src) {
        animateWin(prva1, druga2, treca2)
    }
    // osma 2
    if (prva3_src == druga2_src && druga2_src == treca2_src && treca2_src == cetvrta2_src && cetvrta2_src == peta3_src) {
        animateWin(prva3, druga2, treca2, cetvrta2, peta3)
    } else if (prva3_src == druga2_src && druga2_src == treca2_src && treca2_src == cetvrta2_src) {
        animateWin(prva3, druga2, treca2, cetvrta2)
    } else if (prva3_src == druga2_src && druga2_src == treca2_src) {
        animateWin(prva3, druga2, treca2)
    }


    // deveta 1
    if (prva2_src == druga1_src && druga1_src == treca2_src && treca2_src == cetvrta1_src && cetvrta1_src == peta2_src) {
        animateWin(prva2, druga1, treca2, cetvrta1, peta2)
    } else if (prva2_src == druga1_src && druga1_src == treca2_src && treca2_src == cetvrta1_src) {
        animateWin(prva2, druga1, treca2, cetvrta1)
    } else if (prva2_src == druga1_src && druga1_src == treca2_src) {
        animateWin(prva2, druga1, treca2)
    }
    // deveta 2
    if (prva2_src == druga3_src && druga3_src == treca2_src && treca2_src == cetvrta3_src && cetvrta3_src == peta2_src) {
        animateWin(prva2, druga3, treca2, cetvrta3, peta2)
    } else if (prva2_src == druga3_src && druga3_src == treca2_src && treca2_src == cetvrta3_src) {
        animateWin(prva2, druga3, treca2, cetvrta3)
    } else if (prva2_src == druga3_src && druga3_src == treca2_src) {
        animateWin(prva2, druga3, treca2)
    }


    //deseta 1
    if (prva1_src == druga2_src && druga2_src == treca1_src && treca1_src == cetvrta2_src && cetvrta2_src == peta1_src) {
        animateWin(prva1, druga2, treca1, cetvrta2, peta1)
    } else if (prva1_src == druga2_src && druga2_src == treca1_src && treca1_src == cetvrta2_src) {
        animateWin(prva1, druga2, treca1, cetvrta2)
    } else if (prva1_src == druga2_src && druga2_src == treca1_src) {
        animateWin(prva1, druga2, treca1)
    }
    //deseta 2
    if (prva3_src == druga2_src && druga2_src == treca3_src && treca3_src == cetvrta1_src && cetvrta2_src == peta3_src) {
        animateWin(prva3, druga2, treca3, cetvrta2, peta3)
    } else if (prva3_src == druga2_src && druga2_src == treca3_src && treca3_src == cetvrta2_src) {
        animateWin(prva3, druga2, treca3, cetvrta2)
    } else if (prva3_src == druga2_src && druga2_src == treca3_src) {
        animateWin(prva3, druga2, treca3)
    }


    // jedanaesta 1
    if (prva2_src == druga2_src && druga2_src == treca1_src && treca1_src == cetvrta2_src && cetvrta2_src == peta2_src) {
        animateWin(prva2, druga2, treca1, cetvrta2, peta2)
    } else if (prva2_src == druga2_src && druga2_src == treca1_src && treca1_src == cetvrta2_src) {
        animateWin(prva2, druga2, treca1, cetvrta2)
    } else if (prva2_src == druga2_src && druga2_src == treca1_src) {
        animateWin(prva2, druga2, treca1)
    }
    // jedanaesta 1
    if (prva2_src == druga2_src && druga2_src == treca3_src && treca3_src == cetvrta2_src && cetvrta2_src == peta2_src) {
        animateWin(prva2, druga2, treca3, cetvrta2, peta2)
    } else if (prva2_src == druga2_src && druga2_src == treca3_src && treca3_src == cetvrta2_src) {
        animateWin(prva2, druga2, treca3, cetvrta2)
    } else if (prva2_src == druga2_src && druga2_src == treca3_src) {
        animateWin(prva2, druga2, treca3)
    }


    //dvanaesta 1
    if (prva1_src == druga2_src && druga2_src == treca3_src && treca3_src == cetvrta3_src && cetvrta3_src == peta3_src) {
        animateWin(prva1, druga2, treca3, cetvrta3, peta3)
    } else if (prva1_src == druga2_src && druga2_src == treca3_src && treca3_src == cetvrta3_src) {
        animateWin(prva1, druga2, treca3, cetvrta3)
    } else if (prva1_src == druga2_src && druga2_src == treca3_src) {
        animateWin(prva1, druga2, treca3)
    }
    //dvanaesta 2
    if (prva3_src == druga2_src && druga2_src == treca1_src && treca1_src == cetvrta1_src && cetvrta1_src == peta1_src) {
        animateWin(prva3, druga2, treca1, cetvrta1, peta1)
    } else if (prva3_src == druga2_src && druga2_src == treca1_src && treca1_src == cetvrta1_src) {
        animateWin(prva3, druga2, treca1, cetvrta1)
    } else if (prva3_src == druga2_src && druga2_src == treca1_src) {
        animateWin(prva3, druga2, treca1)
    }

}


function animateWin(box1, box2, box3, box4, box5) {
    brojPogodjenihLinija++;
    var x = arguments.length;
    console.log('broj pogodjenih linija ' + brojPogodjenihLinija);
    if (brojPogodjenihLinija === 1) {
        setTimeout(run, 10)
    }
    if (brojPogodjenihLinija === 2) {
        setTimeout(run, 1000)
    }
    if (brojPogodjenihLinija === 3) {
        setTimeout(run, 2000)
    }
    if (brojPogodjenihLinija === 4) {
        setTimeout(run, 3000)
    }
    if (brojPogodjenihLinija === 5) {
        setTimeout(run, 4000)
    }
    if (brojPogodjenihLinija === 6) {
        setTimeout(run, 5000)
    }
    if (brojPogodjenihLinija === 7) {
        setTimeout(run, 6000)
    }
    if (brojPogodjenihLinija === 8) {
        setTimeout(run, 7000)
    }
    if (brojPogodjenihLinija === 9) {
        setTimeout(run, 8000)
    }
    if (brojPogodjenihLinija === 10) {
        setTimeout(run, 9000)
    }
    if (brojPogodjenihLinija === 11) {
        setTimeout(run, 10000)
    }
    if (brojPogodjenihLinija === 12) {
        setTimeout(run, 11000)
    }
    if (brojPogodjenihLinija === 13) {
        setTimeout(run, 12000)
    }
    if (brojPogodjenihLinija === 14) {
        setTimeout(run, 13000)
    }
    if (brojPogodjenihLinija === 15) {
        setTimeout(run, 14000)
    }
    if (brojPogodjenihLinija === 16) {
        setTimeout(run, 15000)
    }
    if (brojPogodjenihLinija === 17) {
        setTimeout(run, 16000)
    }
    if (brojPogodjenihLinija === 18) {
        setTimeout(run, 17000)
    }
    if (brojPogodjenihLinija === 19) {
        setTimeout(run, 18000)
    }
    if (brojPogodjenihLinija === 20) {
        setTimeout(run, 19000)
    }
    if (brojPogodjenihLinija === 21) {
        setTimeout(run, 20000)
    }

    function run() {
        $('.box').css('background','transparent');
        calculateWin(box1.find('img').attr('src'), x);
        $(box1).css('background', boje[testZaBoje]);
        $(box2).css('background', boje[testZaBoje]);
        $(box3).css('background', boje[testZaBoje]);
        $(box4).css('background', boje[testZaBoje]);
        $(box5).css('background', boje[testZaBoje]);
        testZaBoje++;
    }
}

function checkStake() {
    stake = parseInt(bet.html());
    stakePerLine = Math.round(stake / 21);
    startMoney = parseInt(money.html()) - stake;
    if (startMoney < 210) {
        spin.attr('disabled', 'disabled')
    }
    money.html(startMoney + '$');
}

function upBet() {
    var x = parseInt(bet.html());
    x += 10;
    bet.html(x + '$')
}

function downBet() {
    var x = parseInt(bet.html());
    x -= 10;
    bet.html(x + '$')
}

function calculateWin(champ, howMany) {
    test1++
    console.log("linija " + test1);
    console.log("pogodaka u liniji " + howMany);
    console.log("ulog  >>> "+stakePerLine);

    (champ == 'img/annie.ico') ? basicWin = stakePerLine * 10 : false;
    (champ == 'img/aatrox.ico') ? basicWin = stakePerLine * 10 : false;
    (champ == 'img/akali.ico') ? basicWin = stakePerLine * 10 : false;
    (champ == 'img/brand.ico') ? basicWin = stakePerLine * 10 : false;
    (champ == 'img/draven.png') ? basicWin = stakePerLine * 10 : false;
    (champ == 'img/fiora.ico') ? basicWin = stakePerLine * 10 : false;
    (champ == 'img/lee.ico') ? basicWin = stakePerLine * 10 : false;
    (champ == 'img/thresh.png') ? basicWin = stakePerLine * 10 : false;
    (champ == 'img/zed.png') ? basicWin = stakePerLine * 10 : false;
    (champ == 'img/olaf.ico') ? basicWin = stakePerLine * 10 : false;

    if (howMany == 3) {
        basicWin *= 3;
    }
    if (howMany == 4) {
        basicWin *= 6;
    }
    if (howMany == 5) {
        basicWin *= 15;
    }
    console.log("vrednost pogotka" + basicWin)
    ukupanSpinWin += basicWin;
    win.html(ukupanSpinWin + "$");
    var trenutnoPara = parseInt(money.html()) + basicWin;
    money.html(trenutnoPara);

}